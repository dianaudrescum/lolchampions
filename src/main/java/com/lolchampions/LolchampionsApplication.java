package com.lolchampions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolchampionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LolchampionsApplication.class, args);
    }

}
