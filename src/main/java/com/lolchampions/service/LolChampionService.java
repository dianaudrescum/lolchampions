package com.lolchampions.service;

import com.lolchampions.entities.LolChampion;
import com.lolchampions.repository.LolChampionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LolChampionService {


    @Autowired
    LolChampionsRepository lolChampionRepository;

    //method to retrieve all LolChampion objects
    public List<LolChampion> findAll(){

        return lolChampionRepository.findAll();

    }

    //method to save a LolChampion object
    public LolChampion save(LolChampion lolChampion){
        return lolChampion;
    }

}
