package com.lolchampions.controller;

import com.lolchampions.service.LolChampionService;
import com.lolchampions.entities.LolChampion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lolchampions")
public class LolChampionController {
    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    //private LolChampion lolChampion;
    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll(){

        return lolChampionService.findAll();

    }
    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){

        LOG.debug("Request to create lol champion [" + lolChampion + "]"); //better to put in log file instead
        return this.lolChampionService.save(lolChampion);

    }



    //@PostMapping
    //public LolChampion save(@RequestBody LolChampion lolChampion)
}
