package com.lolchampions.repository;

import com.lolchampions.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface LolChampionsRepository extends JpaRepository <LolChampion ,Long>{

}
